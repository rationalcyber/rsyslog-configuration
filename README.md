# Rsyslog Configuration

Some of the most important data in the enterprise can only come into Splunk as syslog.
Unfortunately, Splunk isn't the best tool for directly receiving syslog data.
Here, we show you how to configure rsyslog to collect, organize, and
prepare your data to be easily ingested by a Splunk forwarder.
Additional information and guidance can be found in our syslog-ng configuration
Wiki, and in out presentation 
(https://conf.splunk.com/files/2018/recordings/exciting-to-be-announced-fn1616.mp4) 
and slides (https://static.rainfocus.com/splunk/splunkconf18/sess/1523501633820001AC3P/finalPDF/Critical-Syslog-Tricks-PartII-1616_1538787172735001ru2d.pdf) from Splunk Conf18.

We hope this helps the community and that you find it useful.  If you have suggested
improvements, please let us know!